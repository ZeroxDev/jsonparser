﻿using System;
using System.Text;
using JsonParser.Scripts.Domain.Model;
using JsonParser.Scripts.Domain.Service;
using JsonParser.Scripts.Infrastructure.Service;
using Moq;
using NUnit.Framework;
using UniRx;

namespace JsonParser.Editor.Tests
{
    [TestFixture]
    public class JsonParserServiceShould 
    {
        private JsonParserService _service;
        private static readonly MembersData EXPECTED_DATA = new MembersData("Test",
            new TableData(new []{"ID", "Name"}, 
                new []
                {
                    new RowData(new []{"001", "Name1"}),
                })
            );

        private Mock<IJsonLoader> _jsonLoader;

        [SetUp]
        public void Setup()
        {
            _jsonLoader = new Mock<IJsonLoader>();
            _service = new JsonParserService(_jsonLoader.Object);

            _jsonLoader.Setup(loader => loader.LoadJson()).Returns(GetTestData());

        }

        [Test]
        public void Parse_Title()
        {
            WhenGettingData()
                .Do(ThenTitleIsParsed)
                .Subscribe();

        }

        [Test]
        public void Parse_Table_Data()
        {
            WhenGettingData()
                .Do(ThenTableIsParsed)
                .Subscribe();
        }

        private IObservable<string> GetTestData()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{")
                .Append("	\"Title\": \"Test\",")
                .Append("	\"ColumnHeaders\": [")
                .Append("		\"ID\",")
                .Append("		\"Name\"")
                .Append("	],")
                .Append("	\"Data\": [{")
                .Append("			\"ID\": \"001\",")
                .Append("			\"Name\": \"Name1\"")
                .Append("		}")
                .Append("	]")
                .Append("}");

            return Observable.Return(sb.ToString());
        }

        private IObservable<MembersData> WhenGettingData()
        {
            return _service.GetData();
        }

        private void ThenTitleIsParsed(MembersData membersData)
        {
            Assert.AreEqual(EXPECTED_DATA.Title, membersData.Title);
        }
        
        private void ThenTableIsParsed(MembersData membersData)
        {
            Assert.AreEqual(EXPECTED_DATA.Table, membersData.Table);
        }
    }
}
