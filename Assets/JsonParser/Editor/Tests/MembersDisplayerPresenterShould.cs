﻿using System;
using JsonParser.Scripts.Domain.Model;
using JsonParser.Scripts.Domain.Service;
using JsonParser.Scripts.Presentation;
using Moq;
using NUnit.Framework;
using UniRx;

namespace JsonParser.Editor.Tests
{
    [TestFixture]
    public class MembersDisplayerPresenterShould 
    {
        private static readonly MembersData MembersData = new MembersData("Test Title", 
                                                                                new TableData(
                                                                                    new string[]{ }, 
                                                                                    new RowData[]{}
                                                                                    ));
        
        private Mock<IParserService> _service;
        private Mock<IMembersDisplayerView> _view;
        private MembersDisplayerPresenter _presenter;

        [SetUp]
        public void Setup()
        {
            _view = new Mock<IMembersDisplayerView>();
            _service = new Mock<IParserService>();
            _presenter = new MembersDisplayerPresenter(_view.Object, _service.Object);

            _service.Setup(service => service.GetData()).Returns(Observable.Return(MembersData));
        }

        [Test]
        public void Reset_View_When_Loading()
        {
            WhenLoading();
            ThenViewIsReset();
        }

        [Test]
        public void Retrieve_Members_Data_And_Show_Title_When_Loading()
        {
            WhenLoading();
            ThenDataIsRetrievedAndTitleIsShown();
        }

        [Test]
        public void Show_Members_Table_When_Loading()
        {
            WhenLoading();
            _view.Verify(view => view.ShowMembersTableWith(MembersData.Table), Times.Once);
        }

        [Test]
        public void Show_Error_Message_When_Failing_Load_Data()
        {
            _service.Setup(s => s.GetData()).Returns(Observable.Throw<MembersData>(new Exception()));


            Assert.Throws<Exception>(WhenLoading);
            _view.Verify(view => view.ShowErrorWith(It.IsAny<string>()), Times.Once());
        }

        private void WhenLoading()
        {
            _presenter.LoadData();
        }

        private void ThenViewIsReset()
        {
            _view.Verify(view => view.Reset(), Times.Once());
        }

        private void ThenDataIsRetrievedAndTitleIsShown()
        {
            _service.Verify(service => service.GetData(), Times.Once());
            _view.Verify(view => view.ShowTitleWith(MembersData.Title), Times.Once());
        }
    }
}
