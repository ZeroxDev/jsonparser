﻿using JsonParser.Scripts.Infrastructure;
using JsonParser.Scripts.Infrastructure.Service;
using JsonParser.Scripts.UnityDelivery.Views;
using UnityEngine;

namespace JsonParser.Scripts.Context
{
    public class AppContext : MonoBehaviour
    {
        [SerializeField] private MembersDisplayerView membersDisplayerView;
        
        void Start()
        {
            var jsonLoader = new AssetBundleJsonLoader();
            var parserService = new JsonParserService(jsonLoader);
            membersDisplayerView.Init(parserService);
        }
    }
}
