using System.Collections.Generic;

namespace JsonParser.Scripts.Domain.Model
{
    public class MembersData
    {
        public string Title { get; }

        public TableData Table { get; }

        public MembersData(string title, TableData table)
        {
            Title = title;
            Table = table;
        }
    }
}