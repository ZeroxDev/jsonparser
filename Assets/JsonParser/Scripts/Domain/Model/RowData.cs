using System.Linq;

namespace JsonParser.Scripts.Domain.Model
{
    public class RowData
    {
        public string[] Items { get; }

        public RowData(string[] items)
        {
            Items = items;
        }

        #region EqualityMembers

        public override bool Equals(object obj)
        {
            if (obj == null)  return false;
            
            var target = (RowData) obj;
            return Items.SequenceEqual(target.Items);
        }

        #endregion
       
    }
}