using System.Linq;

namespace JsonParser.Scripts.Domain.Model
{
    public class TableData
    {
        public string[] Columns { get; }
        public RowData[] Rows { get; }

        public TableData(string[] columns, RowData[] rows)
        {
            Columns = columns;
            Rows = rows;
        }

        #region EqualityMembers

        public override bool Equals(object obj)
        {
            if (obj == null)  return false;
            
            var target = (TableData) obj;
            return Columns.SequenceEqual(target.Columns) && Rows.SequenceEqual(target.Rows);
        }

        #endregion
        
        
    }
}