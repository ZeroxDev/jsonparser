using System;

namespace JsonParser.Scripts.Domain.Service
{
    public interface IJsonLoader
    {
        IObservable<string> LoadJson();
    }
}