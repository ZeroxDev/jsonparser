using System;
using JsonParser.Scripts.Domain.Model;

namespace JsonParser.Scripts.Domain.Service
{
    public interface IParserService
    {
        IObservable<MembersData> GetData();
    }
}