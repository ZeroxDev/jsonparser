using System;
using System.IO;
using JsonParser.Scripts.Domain.Service;
using UniRx;
using UnityEngine;

namespace JsonParser.Scripts.Infrastructure.Service
{
    public class AssetBundleJsonLoader : IJsonLoader
    {
        public IObservable<string> LoadJson()
        {
            var filePath = Path.Combine(Application.streamingAssetsPath, "JsonChallenge.json");
            var json = File.ReadAllText(filePath);
            
            return Observable.Return(json);
        }
    }
}