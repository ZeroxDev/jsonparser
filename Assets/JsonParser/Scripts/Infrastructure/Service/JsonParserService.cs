﻿using System;
using System.Collections.Generic;
using System.Linq;
using JsonParser.Scripts.Domain.Model;
using JsonParser.Scripts.Domain.Service;
using Leguar.TotalJSON;
using UniRx;

namespace JsonParser.Scripts.Infrastructure.Service
{
    public class JsonParserService : IParserService
    {
        private readonly IJsonLoader _jsonLoader;

        public JsonParserService(IJsonLoader jsonLoader)
        {
            _jsonLoader = jsonLoader;
        }

        public IObservable<MembersData> GetData()
        {
            return _jsonLoader.LoadJson().Select(json =>
            {
                var jsonObj = JSON.ParseString(json);
                var membersDataDTO = jsonObj.Deserialize<MembersDataDTO>();

                return membersDataDTO.ToDomainObject();
            });
        }
        
        [System.Serializable]
        private class MembersDataDTO
        {
            [IncludeToJSONSerialize]public string Title;
            [IncludeToJSONSerialize]public string[] ColumnHeaders;
            [IncludeToJSONSerialize]public List<Dictionary<string, string>> Data;
            
            
            public MembersData ToDomainObject()
            {
                var tableData = new TableData(ColumnHeaders, GetDataRows());
                
                return new MembersData(Title, tableData);
            }

            private RowData[] GetDataRows()
            {
                return Data.Select(row => new RowData(row.Values.ToArray())).ToArray();
            }
        }
    }

    
}
