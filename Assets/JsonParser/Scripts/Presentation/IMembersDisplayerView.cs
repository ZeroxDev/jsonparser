using System;
using JsonParser.Scripts.Domain.Model;
using UniRx;

namespace JsonParser.Scripts.Presentation
{
    public interface IMembersDisplayerView
    {
        void ShowTitleWith(string text);
        void ShowMembersTableWith(TableData membersData);
        void Reset();
        void ShowErrorWith(string message);
    }
}