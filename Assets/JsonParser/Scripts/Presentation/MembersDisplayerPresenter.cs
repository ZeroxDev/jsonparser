using System;
using JsonParser.Scripts.Domain.Model;
using JsonParser.Scripts.Domain.Service;
using UniRx;
using UnityEngine;

namespace JsonParser.Scripts.Presentation
{
    public class MembersDisplayerPresenter
    {
        private readonly IMembersDisplayerView _view;
        private readonly IParserService _parserService;

        public MembersDisplayerPresenter(IMembersDisplayerView view, IParserService parserService)
        {
            _view = view;
            _parserService = parserService;
        }

        public void LoadData()
        {
            _view.Reset();

           GetMembersDataAsync()
               .Do(data =>
                {
                    _view.ShowTitleWith(data.Title);
                    _view.ShowMembersTableWith(data.Table);
                })
               .DoOnError(exception =>
               {
                   _view.ShowErrorWith("Error to read json file: " + exception.Message);
               })
               .Subscribe();

        }

        private IObservable<MembersData> GetMembersDataAsync()
        {
            return _parserService.GetData();
        }
    }
}