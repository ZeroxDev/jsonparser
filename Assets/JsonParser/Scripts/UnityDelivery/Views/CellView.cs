﻿using TMPro;
using UnityEngine;

namespace JsonParser.Scripts.UnityDelivery.Views
{
    public class CellView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI cellText;

        public void SetText(string text)
        {
            cellText.text = text;
        }
    }
}
