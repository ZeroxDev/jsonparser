﻿using System;
using JetBrains.Annotations;
using JsonParser.Scripts.Domain.Model;
using JsonParser.Scripts.Domain.Service;
using JsonParser.Scripts.Presentation;
using TMPro;
using UniRx;
using UnityEngine;

namespace JsonParser.Scripts.UnityDelivery.Views
{
    public class MembersDisplayerView : MonoBehaviour, IMembersDisplayerView
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI errorText;
        [SerializeField] private TableView tableView;

        private MembersDisplayerPresenter _presenter;
        
        public void Init(IParserService parserService)
        {
            _presenter = new MembersDisplayerPresenter(this, parserService);
            _presenter.LoadData();
        }
        
        public void ShowTitleWith(string text)
        {
            titleText.text = text;
        }

        public void ShowMembersTableWith(TableData membersData)
        {
            tableView.LoadWith(membersData);
        }

        public void Reset()
        {
            tableView.Clear();
        }

        public void ShowErrorWith(string message)
        {
             Observable.ReturnUnit()
                .Do(_ =>
                {
                    errorText.text = message;
                    errorText.gameObject.SetActive(true);
                })
                .Delay(TimeSpan.FromSeconds(2))
                .Do(_ => errorText.gameObject.SetActive(false))
                .Subscribe();
        }

        [UsedImplicitly]
        public void OnReloadButtonClick()
        {
            _presenter.LoadData();
        }
    }
}
