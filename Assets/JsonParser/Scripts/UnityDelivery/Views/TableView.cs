﻿using JsonParser.Scripts.Domain.Model;
using UnityEngine;
using UnityEngine.UI;

namespace JsonParser.Scripts.UnityDelivery.Views
{
    public class TableView : MonoBehaviour
    {
        [SerializeField] private GridLayoutGroup headerRow;
        [SerializeField] private GridLayoutGroup content;
        [SerializeField] private CellView headerCellPrefab;
        [SerializeField] private CellView cellPrefab;

        public void LoadWith(TableData tableData)
        {
            headerRow.constraintCount = tableData.Columns.Length;
            content.constraintCount = tableData.Columns.Length;
            
            foreach (var columnText in tableData.Columns)
            {
                var headerCell = Instantiate(headerCellPrefab, headerRow.transform);
                headerCell.SetText(columnText);
            }
            
            foreach (var rowData in tableData.Rows)
            {
                foreach (var cellText in rowData.Items)
                {
                    var cell = Instantiate(cellPrefab, content.transform);
                    cell.SetText(cellText);
                }
            }
        }

        public void Clear()
        {
            foreach (Transform child in headerRow.transform)
            {
                Destroy(child.gameObject);
            }
            
            foreach (Transform child in content.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
